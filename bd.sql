create database biblioteca;

use biblioteca;

create table login (
usuario varchar(25),
pass varchar(20)
);

create table libros(
id_libro varchar(25),
nombre_libro varchar(25),
autor varchar(25),
editorial varchar(25),
anio int,
estado varchar(10)
);