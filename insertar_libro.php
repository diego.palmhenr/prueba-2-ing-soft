<?php
include "conexion.php";


$id_libro = $_POST["id_libro"];
$nombre_libro = $_POST["nombre_libro"];
$autor = $_POST["autor"];
$editorial = $_POST["editorial"];
$annio = $_POST["annio"];
$estado = $_POST["estado"];


$c = new conexion();
$c->insertarLibro($id_libro, $nombre_libro, $autor, $editorial, $annio, $estado);
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <!--Etiqueta para el diseño responsivo-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--CSS de Bootstrap y personalizado-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>
        <!--Titulo de la ventana-->
        <title>Registro de libro</title>
    </head>
    <body class="container">
        <!--Jumbotron-->
        <div class="jumbotron jumbotron">
            <div class="container">
                <h1 class="display-4">Libro generado correctamente.</h1>

                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-6">
                        <a href="insertar_libro.html">Añadir un nuevo Libro</a><br>                               
                    </div>
                </div>

                <div class="form-group row justify-content-center align-items-center">
                    <div class="col-6">
                        <a href="mantenedorLibros.html" class="btn btn-info mr-1 my-1">VOLVER</a>                                
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="js/localidades.js" type="text/javascript"></script>
        <script src="js/main.js" type="text/javascript"></script>
    </body>
</html>
